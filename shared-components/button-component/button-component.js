import React from 'react';
import {
	Button
} from 'react-native';

const buttonComponent = ({
    buttonText,
    buttonSubmit
}) => {
	return (
        <Button
            title={buttonText}
            onPress={buttonSubmit}
        />
	);
}

export default buttonComponent;