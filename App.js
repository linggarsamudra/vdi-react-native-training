/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React, {Component} from 'react';
import {
  Platform, 
  StyleSheet, 
  Text, 
  View,
  TextInput,
  Modal,
} from 'react-native';
import {
  ButtonComponent
} from './shared-components';

console.disableYellowBox = true;
console.warn('YellowBox is disabled.');
console.reportErrorsAsExceptions = false;

const instructions = Platform.select({
  ios: 'Press Cmd+R to reload,\n' + 'Cmd+D or shake for dev menu',
  android:
    'Double tap R on your keyboard to reload,\n' +
    'Shake or press menu button for dev menu',
});

export default class App extends Component {


  constructor(props) {
    super(props);
    this.state = {
      inputName: '',
      stringName: 'React Native',
      stringButton: 'Submit',
      modalVisible: false,
    }
  };

  submitButton(value){
    this.setState({stringName: value}, () => {
      this.setState({inputName: '', modalVisible: true});
    });
  }

  render() {
    const {
      inputName,
      stringButton
    } = this.state;
    return (
      <View style={styles.container}>
        <TextInput
          style={{height: 40, borderColor: 'gray', borderWidth: 1, minWidth: 250, marginBottom: 10}}
          onChangeText={(text) => this.setState({inputName: text})}
          value={inputName}
        />
        <ButtonComponent 
          buttonText={stringButton}
          buttonSubmit={() => {
            this.submitButton(inputName);
          }}
        />
        <Text style={[styles.welcome, {color: 'red'}]}>Welcome {this.state.stringName}!</Text>
        <Text style={styles.instructions}>To get started, edit App.js</Text>
        <Text style={styles.instructions}>{instructions}</Text>

        <Modal
          transparent={false}
          visible={this.state.modalVisible}
          onRequestClose={() => { console.log('onRequestClose'); }}
        >
          <View style={styles.container}>
              <Text style={styles.modalTitle}>Data Success!</Text>
              <ButtonComponent 
                buttonText={'Close Modal'}
                buttonSubmit={() => {
                  this.setState({modalVisible: !this.state.modalVisible});
                }}
              />
          </View>
        </Modal>

      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#c3c3c3',
  },
  welcome: {
    fontSize: 20,
    textAlign: 'center',
    margin: 10,
  },
  instructions: {
    textAlign: 'center',
    color: '#333333',
    marginBottom: 5,
  },
  modalTitle: {
    fontSize: 40,
    marginBottom: 10,
  }
});
